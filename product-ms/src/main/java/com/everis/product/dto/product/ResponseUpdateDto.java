package com.everis.product.dto.product;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Data;

@Data
public class ResponseUpdateDto {

    @JsonInclude(Include.NON_NULL)
    private Long codigo;

    private String nombre;

    private String descripcion;
}
