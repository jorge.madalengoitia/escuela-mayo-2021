package com.everis.product.dto.product;

import lombok.Data;

@Data
public class UpdateDto {

    private String nombre;

    private String descripcion;
}
