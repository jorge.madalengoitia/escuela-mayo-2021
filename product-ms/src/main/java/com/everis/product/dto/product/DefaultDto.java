package com.everis.product.dto.product;

import lombok.Data;
import java.util.Date;

@Data
public class DefaultDto {
  private Long codigo;

  private String nombre;

  private String descripcion;

  private Double precio;

  private Double precioConIgv;

  private Boolean activo;

  private Date creado;

}
