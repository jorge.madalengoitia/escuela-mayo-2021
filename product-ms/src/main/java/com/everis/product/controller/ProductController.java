package com.everis.product.controller;

import com.everis.product.dto.product.*;
import com.everis.product.mapper.ProductMapper;
import com.everis.product.service.ProductService;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

@RestController
public class ProductController {

  @Autowired
  private ProductService productService;

  @Autowired
  private ProductMapper productMapper;

  @Value("${igv}")
  private Double igv;

  @GetMapping("/products")
  public List<DefaultDto> getAll(){
    List<DefaultDto> defaultDtoList = new ArrayList<>();
    productService.getAll().forEach(productEntity -> defaultDtoList.add(productMapper.mapEntityToDefaultDto(productEntity, igv)));
    return defaultDtoList;
  }

  @PostMapping("/product")
  public ResponseDto insert(@RequestBody RequestDto request){
    return productMapper.mapEntityToResponseDto(
        productService.save(
            productMapper.mapRequestDtoToEntity(request)));
  }

  @GetMapping("/product/{id}")
  public ResponseGetByIdDto getById(@PathVariable("id") Long id){
    if(id == null || id <= 0){
      throw new IllegalArgumentException("The given id is invalid, the id cannot be null or zero");
    }
    return productMapper.mapEntityToResponseGetByIDto(
        productService.getById(id));
  }

  @GetMapping("/products/insertedToday")
  public List<DefaultDto> findByInsertedToday(){
    List<DefaultDto> defaultDtoList = new ArrayList<>();
    productService.getAllToday().forEach(productEntity -> defaultDtoList.add(productMapper.mapEntityToDefaultDto(productEntity, igv)));
    return defaultDtoList;
  }

  @PutMapping("/product/{id}")
  public ResponseUpdateDto update(@RequestBody UpdateDto request, @PathVariable("id") Long id){
    return productMapper.mapEntityToResponseUpdateDto(productService.save(productMapper.mapUpdateDtoToEntity(request,productService.getById(id))));
  }
}
