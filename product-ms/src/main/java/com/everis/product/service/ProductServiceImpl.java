package com.everis.product.service;

import com.everis.product.entity.ProductEntity;
import com.everis.product.repository.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ProductServiceImpl implements ProductService{

  @Autowired
  private ProductRepository productRepository;

  @Override
  public Iterable<ProductEntity> getAll() {
    return productRepository.findAll();
  }

  @Override
  public ProductEntity getById(Long id) {
    return productRepository.findById(id).orElse(null);
  }

  @Override
  public ProductEntity save(ProductEntity productEntity) {
    return productRepository.save(productEntity);
  }

  @Override
  public void remove(Long id) {
    productRepository.deleteById(id);
  }

  @Override
  public Iterable<ProductEntity> getAllToday(){
    List<ProductEntity> productEntityList = new ArrayList<>();
    productRepository.findAll().forEach(product -> productEntityList.add(product));
    return productEntityList.stream().filter(product -> product.getInsertedAt().getDate() == new Date().getDate()).collect(Collectors.toList());
  }
}
