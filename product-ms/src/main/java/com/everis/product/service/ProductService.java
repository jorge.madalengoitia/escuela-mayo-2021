package com.everis.product.service;

import com.everis.product.entity.ProductEntity;

public interface ProductService {

  public Iterable<ProductEntity> getAll();

  public ProductEntity getById(Long id);

  public ProductEntity save(ProductEntity productEntity);

  public void remove(Long id);

  public Iterable<ProductEntity> getAllToday();

}
