package com.everis.product.mapper;

import com.everis.product.dto.product.DefaultDto;
import com.everis.product.dto.product.RequestDto;
import com.everis.product.dto.product.ResponseDto;
import com.everis.product.dto.product.ResponseGetByIdDto;
import com.everis.product.dto.product.ResponseUpdateDto;
import com.everis.product.dto.product.UpdateDto;
import com.everis.product.entity.ProductEntity;
import java.util.List;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "spring")
public interface ProductMapper {

  @Mapping(source = "id", target = "codigo")
  @Mapping(source = "name", target = "nombre")
  @Mapping(source = "description", target = "descripcion")
  @Mapping(source = "price", target = "precio")
  @Mapping(source = "active", target = "activo")
  ResponseDto mapEntityToResponseDto(ProductEntity productEntity);

  @Mapping(source = "name", target = "nombre")
  @Mapping(source = "description", target = "descripcion")
  @Mapping(source = "price", target = "precio")
  @Mapping(source = "active", target = "activo")
  ResponseGetByIdDto mapEntityToResponseGetByIDto(ProductEntity productEntity);

  @Mapping(source = "nombre", target = "name")
  @Mapping(source = "descripcion", target = "description")
  @Mapping(source = "precio", target = "price")
  @Mapping(constant = "true", target = "active")
  ProductEntity mapRequestDtoToEntity(RequestDto requestDto);

  @Mapping(source = "productEntity.id", target = "codigo")
  @Mapping(source = "productEntity.name", target = "nombre")
  @Mapping(source = "productEntity.description", target = "descripcion")
  @Mapping(source = "productEntity.price", target = "precio")
  @Mapping(expression = "java(productEntity.getPrice() * (igv + 1.0))", target = "precioConIgv")
  @Mapping(source = "productEntity.active", target = "activo")
  @Mapping(source = "productEntity.insertedAt", target = "creado")
  DefaultDto mapEntityToDefaultDto(ProductEntity productEntity, Double igv);

  @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
  @Mapping(source = "nombre", target = "name")
  @Mapping(source = "descripcion", target = "description")
  ProductEntity mapUpdateDtoToEntity(UpdateDto updateDto,@MappingTarget ProductEntity productEntity);

  @Mapping(source = "id", target = "codigo")
  @Mapping(source = "name", target = "nombre")
  @Mapping(source = "description", target = "descripcion")
  ResponseUpdateDto mapEntityToResponseUpdateDto(ProductEntity productEntity);

  List<DefaultDto> mapListEntityToDefaultDto(Iterable<ProductEntity> productEntity);
}
