package com.everis.product.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.refEq;
import static org.mockito.Mockito.when;

import com.everis.product.dto.product.DefaultDto;
import com.everis.product.dto.product.RequestDto;
import com.everis.product.dto.product.ResponseDto;
import com.everis.product.dto.product.ResponseGetByIdDto;
import com.everis.product.dto.product.ResponseUpdateDto;
import com.everis.product.dto.product.UpdateDto;
import com.everis.product.entity.ProductEntity;
import com.everis.product.mapper.ProductMapperImpl;
import com.everis.product.service.ProductService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TestProductController {

  @InjectMocks
  private ProductController productController;

  @Mock
  private ProductService productService;

  @Mock
  private ProductMapperImpl productMapper;

  @Test
  public void testGetAllSuccessfully(){
    ProductEntity productEntity1 = new ProductEntity();
    productEntity1.setId(1L);
    productEntity1.setActive(Boolean.TRUE);
    productEntity1.setName("1");
    productEntity1.setDescription("1");
    productEntity1.setPrice(1D);
    productEntity1.setInsertedAt(new Date());

    DefaultDto defaultDto = new DefaultDto();
    defaultDto.setCodigo(1L);
    defaultDto.setNombre("1");
    defaultDto.setDescripcion("1");
    defaultDto.setPrecio(1D);
    defaultDto.setActivo(Boolean.TRUE);
    defaultDto.setCreado(new Date());
    defaultDto.setPrecioConIgv(1.2);

    when(productMapper.mapEntityToDefaultDto(refEq(productEntity1),any())).thenReturn(defaultDto);
    when(productService.getAll()).thenReturn(List.of(productEntity1));

    assertThat(productController.getAll()).isEqualTo(List.of(defaultDto));
  }

  @Test
  public void testGetAllWhenListEmpty(){
    when(productService.getAll()).thenReturn(new ArrayList<>());

    assertThat(productController.getAll()).isEmpty();
  }

  @Test
  public void testInsertSuccessfully(){
    RequestDto request = new RequestDto();
    request.setNombre("abc");
    request.setDescripcion("descripcion abc");
    request.setPrecio(100D);

    ProductEntity productEntity = new ProductEntity();
    productEntity.setName("abc");
    productEntity.setDescription("descripcion abc");
    productEntity.setPrice(100D);
    productEntity.setActive(Boolean.TRUE);

    ProductEntity productEntitySaved = new ProductEntity();
    productEntitySaved.setId(100L);
    productEntitySaved.setName("abc");
    productEntitySaved.setDescription("descripcion abc");
    productEntitySaved.setPrice(100D);
    productEntitySaved.setActive(Boolean.TRUE);

    ResponseDto responseDto = new ResponseDto();
    responseDto.setNombre("abc");
    responseDto.setCodigo(100L);
    responseDto.setDescripcion("descripcion abc");
    responseDto.setPrecio(100D);
    responseDto.setActivo(Boolean.TRUE);

    when(productMapper.mapRequestDtoToEntity(refEq(request))).thenReturn(productEntity);
    when(productService.save(refEq(productEntity))).
        thenReturn(productEntitySaved);
    when(productMapper.mapEntityToResponseDto(refEq(productEntitySaved))).thenReturn(responseDto);

    assertThat(productController.insert(request)).isEqualTo(responseDto);
  }

  @Test
  public void testGetByIdSuccessfully(){
    ProductEntity productEntity = new ProductEntity();
    productEntity.setId(1L);
    productEntity.setName("name");
    productEntity.setDescription("description");
    productEntity.setActive(Boolean.TRUE);

    when(productService.getById(1L)).thenReturn(productEntity);

    ResponseGetByIdDto responseGetByIdDto = new ResponseGetByIdDto();
    responseGetByIdDto.setNombre("name");
    responseGetByIdDto.setDescripcion("description");
    responseGetByIdDto.setPrecio(null);
    responseGetByIdDto.setActivo(Boolean.TRUE);

    when(productMapper.mapEntityToResponseGetByIDto(productEntity)).thenReturn(responseGetByIdDto);

    assertThat(productController.getById(1L)).isEqualTo(responseGetByIdDto);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testGetByIdWhenIdNull(){
    productController.getById(null);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testGetByIdWhenIdZero(){
    productController.getById(0L);
  }

  @Test
  public void testFindByInsertedTodaySuccessfully(){
    ProductEntity productEntity = new ProductEntity();
    productEntity.setId(1L);
    productEntity.setName("name");
    productEntity.setDescription("description");
    productEntity.setPrice(1D);
    productEntity.setActive(Boolean.TRUE);
    productEntity.setInsertedAt(new Date());

    DefaultDto defaultDto = new DefaultDto();
    defaultDto.setCodigo(1L);
    defaultDto.setNombre("name");
    defaultDto.setDescripcion("description");
    defaultDto.setPrecio(1D);
    defaultDto.setPrecioConIgv(1.2);
    defaultDto.setActivo(Boolean.TRUE);
    defaultDto.setCreado(new Date());

    when(productMapper.mapEntityToDefaultDto(productEntity, null)).thenReturn(defaultDto);
    when(productService.getAllToday()).thenReturn(List.of(productEntity));

    List<DefaultDto> defaultDtoList = productController.findByInsertedToday();

    assertThat(defaultDtoList.get(0).getCodigo()).isEqualTo(1L);
    assertThat(defaultDtoList.get(0).getNombre()).isEqualTo("name");
    assertThat(defaultDtoList.get(0).getDescripcion()).isEqualTo("description");
    assertThat(defaultDtoList.get(0).getPrecio()).isEqualTo(1D);
    assertThat(defaultDtoList.get(0).getActivo()).isTrue();
    assertThat(defaultDtoList.get(0).getCreado()).isEqualToIgnoringSeconds(new Date());
  }

  @Test
  public void testFindByInsertedTodayWhenListEmpty(){
    when(productService.getAllToday()).thenReturn(new ArrayList<>());
    assertThat(productController.findByInsertedToday()).isEmpty();
  }

  @Test
  public void testUpdateSuccesfully(){
    UpdateDto request = new UpdateDto();
    request.setNombre("1");
    request.setDescripcion("1");

    ProductEntity productEntity = new ProductEntity();
    productEntity.setId(1L);
    productEntity.setName("name");
    productEntity.setDescription("description");
    productEntity.setActive(Boolean.TRUE);
    productEntity.setInsertedAt(new Date());

    ProductEntity productEntityUpdated = new ProductEntity();
    productEntityUpdated.setId(1L);
    productEntityUpdated.setName("1");
    productEntityUpdated.setDescription("1");
    productEntityUpdated.setActive(Boolean.TRUE);
    productEntityUpdated.setInsertedAt(new Date());

    ResponseUpdateDto responseUpdateDto = new ResponseUpdateDto();
    responseUpdateDto.setCodigo(1L);
    responseUpdateDto.setNombre("1");
    responseUpdateDto.setDescripcion("1");

    when(productService.getById(1L)).thenReturn(productEntity);
    when(productMapper.mapUpdateDtoToEntity(request,productEntity)).thenReturn(productEntityUpdated);
    when(productMapper.mapEntityToResponseUpdateDto(productEntityUpdated)).thenReturn(responseUpdateDto);
    when(productService.save(refEq(productEntityUpdated))).thenReturn(productEntityUpdated);

    assertThat(productController.update(request,1L)).isEqualTo(responseUpdateDto);
  }
}
