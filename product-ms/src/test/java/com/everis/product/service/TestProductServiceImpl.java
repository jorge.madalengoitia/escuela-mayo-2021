package com.everis.product.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.refEq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.everis.product.entity.ProductEntity;
import com.everis.product.repository.ProductRepository;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TestProductServiceImpl {

  @InjectMocks
  private ProductServiceImpl productService;

  @Mock
  private ProductRepository productRepository;

  @Test
  public void testGetAllSuccessfully(){
    ProductEntity productEntity = new ProductEntity();
    productEntity.setId(1L);
    productEntity.setName("name");
    productEntity.setDescription("description");
    productEntity.setPrice(99D);
    productEntity.setActive(Boolean.FALSE);
    List<ProductEntity> productEntityList = List.of(productEntity);

    when(productRepository.findAll()).thenReturn(productEntityList);

    Iterable<ProductEntity> productEntityIterable = productService.getAll();

    assertThat(productEntityIterable).hasSize(1);
    assertThat(productEntityIterable.iterator().next()).isEqualTo(productEntity);
  }

  @Test
  public void testGetByIdSuccessfully(){
    ProductEntity productEntity = new ProductEntity();
    productEntity.setId(1L);
    productEntity.setName("name");
    productEntity.setDescription("description");
    productEntity.setPrice(99D);
    productEntity.setActive(Boolean.FALSE);

    when(productRepository.findById(anyLong())).thenReturn(Optional.of(productEntity));

    assertThat(productService.getById(99L)).isEqualTo(productEntity);
  }

  @Test
  public void testSaveSuccessfully(){
    ProductEntity productEntity = new ProductEntity();
    productEntity.setId(1L);
    productEntity.setName("name");
    productEntity.setDescription("description");
    productEntity.setPrice(99D);
    productEntity.setActive(Boolean.FALSE);

    when(productRepository.save(refEq(productEntity))).thenReturn(productEntity);

    assertThat(productService.save(productEntity)).isEqualTo(productEntity);
  }

  @Test
  public void testRemoveSuccessfully(){
    productService.remove(99L);
    verify(productRepository, times(1)).deleteById(anyLong());
  }

  @Test
  public void testGetAllToday(){
    ProductEntity productEntity = new ProductEntity();
    productEntity.setId(1L);
    productEntity.setName("name");
    productEntity.setDescription("description");
    productEntity.setPrice(99D);
    productEntity.setActive(Boolean.FALSE);
    productEntity.setInsertedAt(new Date());
    List<ProductEntity> productEntityList = List.of(productEntity);

    when(productRepository.findAll()).thenReturn(productEntityList);

    assertThat(productService.getAllToday().iterator().next().getInsertedAt()).isToday();
  }

}
