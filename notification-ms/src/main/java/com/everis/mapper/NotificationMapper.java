package com.everis.mapper;

import com.everis.dto.RequestDto;
import com.everis.entity.NotificationEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.mail.SimpleMailMessage;

@Mapper(componentModel = "spring")
public interface NotificationMapper {

    @Mapping(expression = "java(request.getMessage())", target = "mensaje")
    @Mapping(expression = "java(request.getCliente().getCorreo())", target = "correo")
    @Mapping(expression = "java(java.time.LocalDateTime.now())", target = "fecha")
    NotificationEntity mapRequestDtoToEntity(RequestDto request);

    @Mapping(source = "remitentes", target = "to")
    @Mapping(constant = "Orden creada", target = "subject")
    @Mapping(expression = "java(requestDto.getMessage())", target = "text")
    SimpleMailMessage mapRequestDtoToEmail(RequestDto requestDto, String[] remitentes);
}
