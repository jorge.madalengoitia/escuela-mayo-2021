package com.everis.service;

import com.everis.dto.RequestDto;
import com.everis.entity.NotificationEntity;
import com.everis.mapper.NotificationMapper;
import com.everis.repository.NotificationRepository;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;


@Slf4j
@Service
public class NotificationServiceImpl implements NotificationService{

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private NotificationRepository notificationRepository;

    @Autowired
    private NotificationMapper notificationMapper;

    @Override
    @KafkaListener(topics = "order")
    public void sendEmail(String jsonSource) {
        Gson gson = new GsonBuilder().create();
        RequestDto requestDto = gson.fromJson(jsonSource, RequestDto.class);
        String[] remitentes = {requestDto.getCliente().getCorreo()};
        SimpleMailMessage email = notificationMapper.mapRequestDtoToEmail(requestDto,remitentes);
        mailSender.send(email);
        log.info("******** MENSAJE ENVIADO {} **********", email);
        notificationRepository.save(notificationMapper.mapRequestDtoToEntity(requestDto));
    }

    @Override
    public NotificationEntity save(NotificationEntity notificationEntity) {
        return notificationRepository.save(notificationEntity);
    }

}
