package com.everis.service;

import com.everis.dto.RequestDto;
import com.everis.entity.NotificationEntity;

public interface NotificationService {
    public void sendEmail(String jsonSource);
    public NotificationEntity save(NotificationEntity notificationEntity);
}
