package com.everis.repository;

import com.everis.dto.RequestDto;
import com.everis.entity.NotificationEntity;
import org.mapstruct.Mapping;
import org.springframework.data.repository.CrudRepository;

public interface NotificationRepository extends CrudRepository<NotificationEntity, Long> {
}
