package com.everis.dto;

import lombok.Data;

@Data
public class RequestDetailsDto {

    private Long productoCodigo;
    private Integer cantidad;
    private Double precio;

}
