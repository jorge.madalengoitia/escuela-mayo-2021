package com.everis.dto;

import lombok.*;

import java.util.List;

@Data
public class RequestDto {
    private Long idOrden;
    private RequestClienteDto cliente;
    private List<RequestDetailsDto> orderDetailsDto;

    public String getMessage(){
        return "Hola "+ getCliente().getNombre() + " "+ getCliente().getApellido() + "\n" +
                         "la orden numero "+ getIdOrden() + " ya esta siendo procesada. \n\n" +
                         "Tu lista de pedido es \n"+ getOrderDetailsDto().toString();
    }
}
