package com.everis.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class RequestClienteDto {
    private String nombre;
    private String apellido;
    private String correo;

}
