package com.everis.order.service;

import com.everis.order.dto.order.ResponseOrderDto;
import com.everis.order.entity.OrderEntity;


public interface OrderService {

    public OrderEntity save(OrderEntity orderEntity);

    public void sendMessage(ResponseOrderDto message, String topic);
}