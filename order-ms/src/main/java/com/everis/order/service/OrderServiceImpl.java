package com.everis.order.service;

import com.everis.order.dto.order.ResponseOrderDto;
import com.everis.order.respository.OrderRepository;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.everis.order.entity.OrderEntity;

@Slf4j
@Service
public class OrderServiceImpl implements OrderService{

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private KafkaTemplate producer;

    @Override
    public OrderEntity save(OrderEntity orderEntity) {
        return orderRepository.save(orderEntity);
    }

    @Override
    public void sendMessage(ResponseOrderDto message, String topic) {
        producer.send(topic, new Gson().toJson(message));
        log.info("Sending messsage: {}", message);
    }
}
