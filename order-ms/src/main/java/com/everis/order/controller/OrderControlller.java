package com.everis.order.controller;

import com.everis.order.OrderMapper.OrderMapper;
import com.everis.order.dto.order.RequestOrderDto;
import com.everis.order.dto.order.ResponseOrderDto;
import com.everis.order.service.OrderService;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderControlller {

    @Autowired
    private OrderService orderService;

    @Autowired
    private OrderMapper orderMapper;

    @Value("${kafka.topic.default}")
    private String defaultTopic;

    @PostMapping("/order")
    public ResponseOrderDto insert(@RequestBody RequestOrderDto request) {
        ResponseOrderDto responseOrderDto = orderMapper.mapEntityToResponseOrderDto(
                orderService.save(orderMapper.mapRequestDtoToEntityList(request)));
        orderService.sendMessage(responseOrderDto , defaultTopic);
        return responseOrderDto;
    }

    @Bean
    public NewTopic topicAdvice(){
        return new NewTopic(defaultTopic, 3, (short) 1);
    }
}
