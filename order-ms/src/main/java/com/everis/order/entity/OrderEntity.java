package com.everis.order.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import lombok.Getter;
import lombok.Setter;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Getter
@Setter
public class OrderEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idOrder;

    private LocalDateTime transactionDate;

    private Boolean active;

    @JsonIgnore
    @ManyToOne( cascade = { CascadeType.ALL } )
    @JoinColumn(name = "idClient", nullable = false, foreignKey = @ForeignKey(name = "FK_client"))
    private ClientEntity client;

    @OneToMany(mappedBy = "order", cascade = { CascadeType.ALL })
    private List<OrderDetailsEntity> orderDetailsEntity;

}
