package com.everis.order.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class OrderDetailsEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idDetail;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "idOrder", nullable = false, foreignKey = @ForeignKey(name = "FK_order_detail"))
    private OrderEntity order;

    private Long productId;

    private Integer quantity;

    private Double price;
}
