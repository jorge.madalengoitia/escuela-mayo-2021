package com.everis.order.dto.orderDetail;

import lombok.Data;

@Data
public class ResponseDetailsDto {

    private Long productoCodigo;

    private Integer cantidad;

    private Double precio;
}
