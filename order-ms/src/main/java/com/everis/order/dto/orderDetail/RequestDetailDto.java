package com.everis.order.dto.orderDetail;

import com.everis.order.entity.OrderEntity;
import lombok.Data;


@Data
public class RequestDetailDto {

    private OrderEntity orden;

    private Long productoCodigo;

    private Integer cantidad;

    private Double precio;
}
