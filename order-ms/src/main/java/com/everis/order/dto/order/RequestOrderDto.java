package com.everis.order.dto.order;

import com.everis.order.dto.client.RequestClientDto;
import com.everis.order.dto.orderDetail.RequestDetailDto;
import lombok.Data;

import java.util.List;

@Data
public class RequestOrderDto {

    private RequestClientDto cliente;

    private List<RequestDetailDto> orderDetailsDto;

}
