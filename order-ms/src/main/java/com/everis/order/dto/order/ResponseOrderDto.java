package com.everis.order.dto.order;

import com.everis.order.dto.client.ResponseClientDto;
import com.everis.order.dto.orderDetail.ResponseDetailsDto;
import lombok.Data;

import java.util.List;

@Data
public class ResponseOrderDto {

    private Long idOrden;

    private ResponseClientDto cliente;

    private List<ResponseDetailsDto> orderDetailsDto;
}