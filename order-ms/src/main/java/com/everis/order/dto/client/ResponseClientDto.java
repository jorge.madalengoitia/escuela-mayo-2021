package com.everis.order.dto.client;

import lombok.Data;

@Data
public class ResponseClientDto {

    private String nombre;

    private String apellido;

    private String correo;
}
