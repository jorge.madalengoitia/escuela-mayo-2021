package com.everis.order.OrderMapper;

import com.everis.order.dto.client.RequestClientDto;
import com.everis.order.dto.client.ResponseClientDto;
import com.everis.order.dto.order.RequestOrderDto;
import com.everis.order.dto.order.ResponseOrderDto;
import com.everis.order.dto.orderDetail.RequestDetailDto;
import com.everis.order.dto.orderDetail.ResponseDetailsDto;
import com.everis.order.entity.ClientEntity;
import com.everis.order.entity.OrderDetailsEntity;
import com.everis.order.entity.OrderEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Mapper(componentModel = "spring")
public interface OrderMapper {

    // Request To Entity

    @Mapping(source = "requestDetailDto.productoCodigo", target = "productId")
    @Mapping(source = "requestDetailDto.cantidad", target = "quantity")
    @Mapping(source = "requestDetailDto.precio", target = "price")
    @Mapping(source = "orderEntity", target = "order")
    OrderDetailsEntity mapRequestDetailDtoToEntity(RequestDetailDto requestDetailDto, OrderEntity orderEntity);


    @Mapping(source = "nombre", target = "name")
    @Mapping(source = "apellido", target = "lastName")
    @Mapping(source = "correo", target = "email")
    ClientEntity mapRequestClientDtoToEntity(RequestClientDto requestClientDto);


    @Mapping(source = "now", target = "transactionDate")
    @Mapping(constant = "true", target = "active")
    OrderEntity mapRequestDtoToEntity(RequestOrderDto requestOrderDto, LocalDateTime now);


    default OrderEntity mapRequestDtoToEntityList(RequestOrderDto requestOrderDto){
        OrderEntity orderEntity = mapRequestDtoToEntity(requestOrderDto, LocalDateTime.now());

        ClientEntity clientEntity = mapRequestClientDtoToEntity(requestOrderDto.getCliente());
        clientEntity.setOrder(List.of(orderEntity));

        List<OrderDetailsEntity> list = new ArrayList<>();

        requestOrderDto.getOrderDetailsDto().forEach(detalle -> {
             list.add(mapRequestDetailDtoToEntity(detalle, orderEntity));
        });

        orderEntity.setClient(clientEntity);
        orderEntity.setOrderDetailsEntity(list);
        return orderEntity;
    }

    // Entity To Response

    @Mapping(source = "name", target = "nombre")
    @Mapping(source = "lastName", target = "apellido")
    @Mapping(source = "email", target = "correo")
    ResponseClientDto mapEntityToResponseClientDto(ClientEntity clientEntity);

    @Mapping(source = "productId", target = "productoCodigo")
    @Mapping(source = "quantity", target = "cantidad")
    @Mapping(source = "price", target = "precio")
    ResponseDetailsDto mapEntityToResponseDetailsDto(OrderDetailsEntity orderDetailsEntity);

    List<ResponseDetailsDto> mapEntityToListResponseDetailsDto(List<OrderDetailsEntity> orderDetailsEntities);

    @Mapping(source = "idOrder", target = "idOrden")
    @Mapping(source = "client", target = "cliente")
    @Mapping(source = "orderDetailsEntity", target = "orderDetailsDto")
    ResponseOrderDto mapEntityToResponseOrderDto(OrderEntity orderEntity);


}
